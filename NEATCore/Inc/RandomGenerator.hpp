#pragma once

#include <memory>

class RandomGenerator
{
public:
    RandomGenerator();
    RandomGenerator(const RandomGenerator&);
    virtual ~RandomGenerator() = default;

    virtual float get_next(uint16_t rand_max = 1) const;
};
